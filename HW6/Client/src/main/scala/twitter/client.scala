package twitter

import akka.actor.{Actor, ActorRef, ActorSystem, Props, PoisonPill}
import com.typesafe.config.ConfigFactory
import akka.event.Logging
import scala.collection.mutable.Set

object Client extends App {

  val system = ActorSystem("Twitter",
      ConfigFactory.load.getConfig("client"))
  val userName = args(0)
  val processor = system.actorOf(Props(classOf[ClientProcessor], userName),
                                     name = "client_" + userName)
  processCommands
  println("\nSystem shutdown")

  def processCommands(): Unit = {
    for (ln <- io.Source.stdin.getLines) {
      ln match {
        case "quit" => processor ! ClientCommand(ln)
                       return
        case _ => processor ! ClientCommand(ln)
      }
    }
  }
}

class ClientProcessor(userName: String) extends Actor {

  val server = context.actorSelection("akka.tcp://Twitter@127.0.0.1:2552/user/server_actor")
  val log = Logging(context.system, this)

  // This client's RegisteredUser actor on the Server
  var registeredUser: ActorRef = null

  val following: Set[String] = Set()

  server ! Connect(userName)
  println("Client " + userName + " is alive!")

  def receive = {
    case ClientCommand(cmd)   => 
          cmd match {
            case "following"                          => printListMessage("Following", following.toList)
            case "list"                               => if (isConnected) server ! ListUsers
                                                         else println("Not connected")

            case cmd if (cmd.startsWith("follow") ||
                         cmd.startsWith("unfollow"))  =>  processTwoTokenCommand(cmd) 

            case cmd if (cmd.startsWith("tweet"))     => processTweetCommand(cmd)

            case "quit"                               => if (isConnected)
                                                           server ! Disconnect(userName)
                                                         context.stop(self)
                                                         context.system.shutdown
                                                         System.exit(0)

            case _                                    => println("Command not understood")
          }
          println
    case Connected(regUser, msgs)   =>  println("Recieved connection ack, my registered user is "
                                                  + regUser.path)
                                        registeredUser = regUser
                                        msgs.foreach((t: Tweeted) => printTweet(t.handle, t.message))

    case Error(message)             =>  log.error(message)

    case RegisteredUsers(users)     =>  printListMessage("Registered Users", users.toList)

    case Followed(handle)           =>  println("You are now following " + handle)
                                        following += handle

    case Unfollowed(handle)         =>  println("You are no longer following " + handle)
                                        following -= handle

    case Tweeted(handle, message)   =>  printTweet(handle, message)
  }

  def isConnected(): Boolean = registeredUser != null

  def printListMessage(title: String, items: List[String]): Unit = {
    println(title)
    println("-" * title.length)
    items.foreach(println _)
    println
  }

  def printTweet(handle: String, message: String): Unit = {
    val heading = handle + " tweets:"
    println(heading)
    println( "-" * heading.length)
    println(message)
  }

  def processTwoTokenCommand(commandString: String): Unit = {
    if (isConnected) {
      val tokens = commandString.split("\\s+")
        if (tokens.size == 2) {
          val command = tokens(0)
          val handle = tokens(1) 
            if (handle != userName)
              server ! (command match {
                          case "follow" => Follow(handle, registeredUser)
                          case "unfollow" => Unfollow(handle, registeredUser)
                        })
            else println("You can't " + command + " yourself.")
        }
        else 
          println("Usage: <command> <handle>")
    }
    else println("Not connected")
  }

  def processTweetCommand(commandString: String): Unit = {
    if (isConnected) {
      val tokens = commandString.split("tweet\\s+")
        if (tokens.size == 2) {
          val tweetMsg = tokens(1) 
            registeredUser ! Tweet(tweetMsg)
        }
        else 
          println("Usage: tweet <message>")
    }
    else println("Not connected")
  }
}
