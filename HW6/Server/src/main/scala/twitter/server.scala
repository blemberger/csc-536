package twitter

import akka.actor.{Actor, ActorSystem, ActorRef, Props}
import akka.event.Logging
import com.typesafe.config.ConfigFactory
import scala.collection.mutable.Map
import scala.collection.mutable.Set
import scala.collection.mutable.Queue

object Server extends App {

  val system = ActorSystem("Twitter", ConfigFactory.load.getConfig("server"))
  system.actorOf(Props[ServerActor], name = "server_actor")
}

class ServerActor extends Actor {

  val log = Logging(context.system, this)

  // Registered users in the system
  // Mapping is username -> RegisteredUser
  var registered: Map[String, ActorRef] = Map() 

  // Currently connected users, a set of RegisteredUsers
  var connected: Set[ActorRef] = Set() 

  println("ServerActor is alive!")

    def receive = {
      case Connect(userName) =>  println("Received connection from " + userName) 
                                 val userLookup = registered.get(userName)
                                 if (userLookup == None) // userName is not already registered
                                   registered += (userName -> context.actorOf(Props(classOf[RegisteredUser],
                                                                userName), name = "registered_" + userName))
                                 // userName is now registered
                                 val registeredUser = registered(userName)
                                 if (connected.contains(registeredUser)) {
                                   val msg = userName + " is already logged in elsewhere."
                                   log.error(msg)
                                   sender ! Error(msg)
                                 }
                                 else { 
                                   println(userName + " has connected.")
                                   connected += registeredUser 
                                   registeredUser ! ClientConnect(userName, sender)
                                 }
      case Disconnect(userName) =>  println("Received disconnection from " + userName) 
                                    val userLookup = registered.get(userName)
                                    if (userLookup != None) {
                                       val registeredUser = registered(userName)
                                       connected -= registeredUser
                                       registeredUser ! Disconnect(userName)                                       
                                    }
      case ListUsers => sender ! RegisteredUsers(registered.keys.toList)

      case Follow(handle, followerUser) => if (registered.get(handle) != None) {
                                             // Send Following message to the user corresponding to handle
                                             registered(handle) ! FollowingYou(followerUser)
                                             sender ! Followed(handle) // Acknowledge following
                                           }
                                           else sender ! Error("no such user: " + handle)
      case Unfollow(handle, followerUser) => if (registered.get(handle) != None) {
                                             // Send Unfollowing message to the user corresponding to handle
                                             registered(handle) ! UnfollowingYou(followerUser)
                                             sender ! Unfollowed(handle) // Acknowledge unfollowing
                                           }
                                           else sender ! Error("no such user: " + handle)
    }
}

class RegisteredUser(userName: String) extends Actor {

  var myClient: ActorRef = null
  val log = Logging(context.system, this)
  val queuedMessages: Queue[Tweeted] = Queue()
  val followers: Set[ActorRef] = Set() // Set of other RegisteredUsers that are following this user

  def receive = {
    case ClientConnect(_, client) =>  myClient = client
                                      // Acknowledge connection with client actor
                                      myClient ! Connected(self,
                                                   queuedMessages.dequeueAll((_) => true).toList) 

    case Disconnect(_)            =>  myClient = null 

    case FollowingYou(follower)   =>  if (!followers.contains(follower)) {
                                        followers += follower
                                        println(userName + " now being followed by " + follower.path)
                                      }

    case UnfollowingYou(follower) =>  if (followers.contains(follower)) {
                                        followers -= follower
                                        println(userName + " no longer being followed by " + follower.path)
                                      }

    case Tweet(message)           =>  followers.foreach( _ ! Tweeted(userName, message))

    case tweet @ Tweeted(_, _)    =>  if (myClient == null)
                                        // Client is not connected, so queue tweeted
                                        // messages for later retrieval
                                        queuedMessages += tweet
                                      else myClient ! tweet
  }
}

