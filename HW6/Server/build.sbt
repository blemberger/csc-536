name := "Homework 6 Twitter"

scalaVersion := "2.10.3"

resolvers += "Typesafe Repository" at "http://repo.typesafe.com/typesafe/releases/"

libraryDependencies += "com.typesafe.akka" %% "akka-actor" % "2.3.1"

libraryDependencies += "com.typesafe.akka" %% "akka-remote" % "2.3.1"

unmanagedSourceDirectories in Compile += baseDirectory.value / ".." / "Common"
