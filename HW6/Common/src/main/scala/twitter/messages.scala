package twitter

import akka.actor.ActorRef

case class ClientCommand(command: String)
case class Connect(userName: String)
case class ClientConnect(userName: String, client: ActorRef)
case class Disconnect(userName: String)
case class Error(message: String)
case class Connected(registeredUser: ActorRef, missedMsgs: List[Tweeted])
case object ListUsers 
case class RegisteredUsers(users: List[String])
case class Follow(handle: String, followerUser: ActorRef)
case class Unfollow(handle: String, followerUser: ActorRef)
case class FollowingYou(followerUser: ActorRef)
case class UnfollowingYou(followerUser: ActorRef)
case class Followed(handle: String)
case class Unfollowed(handle: String)
case class Tweet(message: String)
case class Tweeted(handle: String, message: String)
