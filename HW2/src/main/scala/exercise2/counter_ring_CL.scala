package exercise2

import scala.collection.mutable.Map
import scala.util.Random
import akka.actor.{Actor, ActorRef, ActorSystem, Props}

class RingMessage // RingMessages are messages specific to the token ring application

case object TOKEN extends RingMessage

/**
 * Initialization message is passed around the ring on startup to inform
 * each node of its next-neighbors.
 */
case class Init(sendees: List[(ActorRef, ActorRef)]) extends RingMessage {
  override def toString = "Init" 
}

/**
 * Marker messages are only used for C-L snapshot algorithm
 */
case class Marker()

/**
 *  CounterNode is a node in a ring topology, participating in:
 *    1) uni-directional token passing, token moves CCW around ring 
 *    2) bi-directional Chandy-Lamport global state snapshot algorithm
 */ 
class CounterNode extends Actor {

  // Node state for token-ring application
  var counter = 0L
  var tokenSendee: ActorRef = null // this actor always sends tokens to
  var tokenSender: ActorRef = null // always sends this actor tokens

  // Node state for Chandy-Lamport snapshot 
  var channelsRecord: Map[ActorRef, List[RingMessage]] = null
  var channelsRecordStore: Map[ActorRef, List[RingMessage]] = null
  var recordedCounter = 0L

  def receive = {
    case TOKEN => 
      counter += 1
      println(self.path.name + " counter: " + counter)
      recordRingMessage(TOKEN)
      Thread.sleep(Server.calculateDelay(1000, 250)) // between .25 and 1 sec
      tokenSendee ! TOKEN
    case msg @ Init(neighbors) => neighbors match {
      case (n1, n2) :: Nil =>
        tokenSendee = n2
        tokenSender = n1
      case (n1, n2) :: remainingNeighbors =>
        tokenSendee = n2
        tokenSender = n1
        tokenSendee ! Init(remainingNeighbors)
      }
      recordRingMessage(msg)
      println(self.path.name + " will send tokens to " + tokenSendee.path.name
              + " and will receive tokens from " + tokenSender.path.name)
    case Marker() => 
      if (channelsRecord == null) {
        beginRecordingChannels
        recordedCounter = counter // Record state of counter
        tokenSender ! Marker()
        tokenSendee ! Marker()
      }
      if (senderIsNode) storeChannelState(sender)
      if (channelsRecordStore.size == channelsRecord.size) {
        printNodeState
        stopRecording
      }
  }

  /**
   * Add the RingMessage to the channelsRecord Map, only if we are recording.
   */
  def recordRingMessage(msg: RingMessage): Unit = {
    if (channelsRecord != null && senderIsNode)
        channelsRecord(sender) = channelsRecord(sender) :+ msg
  }

  /**
   * Begin recording on all of this node's incoming channels for C-L snapshot
   * algorithm. The channelsRecordStore map gets initialized to indicate that
   * recording has begun.
   */
  def beginRecordingChannels(): Unit = {
      if (tokenSender == null || tokenSendee == null)
        throw new RuntimeException("Unable to begin snapshot, " + self.path.name +
                  " is not initialized")
      channelsRecord = Map(tokenSender -> Nil, tokenSendee -> Nil)
      channelsRecordStore = Map()
  }      

  /**
   * When a Marker message arrives on an inbound channel and recording is in progress,
   * recording on that channel needs to be closed. This is done by copying the contents
   * of the channelsRecord map for that channel to a new map.
   */
  def storeChannelState(markerSender: ActorRef): Unit = {
    channelsRecordStore += (markerSender -> channelsRecord(markerSender))  
  }

  /**
   * Stop recording on all of this node's incoming channels for C-L snapshot
   * algorithm. The channelRecordStore map is set to null to indicate that recording
   * has ended.
   */
  def stopRecording(): Unit = {
    channelsRecordStore = null
    channelsRecord = null
    recordedCounter = 0L
  } 

  /**
   * Print to console the node's recorded token counter as well as all messages
   * recorded on it's incoming channels while the snapshot algorithm was recording.
   */
  def printNodeState(): Unit = {
    var stateMsg = "\n" + "*" * 10 + " " + self.path.name + " state " + "*" * 10 + "\n"
    stateMsg += "counter: " + recordedCounter + "\n"
    channelsRecord.filter((e: (ActorRef, List[RingMessage])) => !e._2.isEmpty)
                  .foreach((e: (ActorRef, List[RingMessage])) => {
                            e._2.foreach((msg: RingMessage) => 
                                         stateMsg += msg + " on segment "
                                                     + e._1.path.name 
                                                     + " -> " + self.path.name + "\n") 
                  })
    stateMsg += "*" * 33 + "\n\n"
    print(stateMsg)
  }

  /**
   * True if the sender of the last message recieved by this node was another node
   * in the ring.
   */
  def senderIsNode(): Boolean = (sender != context.system.deadLetters)
}

/**
 * Main Application
 */
object Server extends App {
  val system = ActorSystem("CounterChandyLamportRing")
    val first = system.actorOf(Props[CounterNode], name = "node1")
    val second = system.actorOf(Props[CounterNode], name = "node2")
    val third = system.actorOf(Props[CounterNode], name = "node3")
    println(first.path)
    println(second.path)
    println(third.path)
    first ! Init(List((third, second),(first, third),(second, first)))
    first ! TOKEN
    println("Server ready")
    Thread.sleep(calculateDelay(10000, 5000)) // between 5 and 10 sec
    third ! Marker()
    println("Snapshot Started!")

    /**
     * Calculate a random Int between minDelay and maxDelay for
     * the purpose of delaying the sending of a message.
     */
    def calculateDelay(maxDelay: Int, minDelay: Int): Int = {
      new Random().nextInt(maxDelay-minDelay) + minDelay 
    }
}

