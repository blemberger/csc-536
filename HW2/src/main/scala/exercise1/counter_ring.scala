package exercise1 

import akka.actor.Actor
import akka.actor.ActorRef
import akka.actor.ActorSystem
import akka.actor.Props

case object TOKEN 

/**
 * Initialization message is passed around the ring on startup to inform
 * each node of the nodes that it is to send tokens to.
 */
case class Init(sendees: List[ActorRef])

/**
 * CounterNode is a node in a ring topology, participating in uni-directional
 * token passing, token moves CCW around ring.
 */ 
class CounterNode extends Actor {

  var counter = 0L
  var sendee: ActorRef = null

    def receive = {
      case TOKEN => 
        counter += 1
        println(self.path.name + " counter: " + counter)
        Thread.sleep(500)
        // sendee is always guaranteed to be initialized, because
        // each sender always sends Init before TOKEN to its sendee  
        sendee ! TOKEN
      case Init(sendees) => sendees match {
            case node :: Nil =>
              sendee = node 
            case node :: remainingNodes =>
              sendee = node 
              sendee ! Init(remainingNodes)
            }
            println(self.path.name + " will send tokens to " + sendee.path.name)
    }
}

/**
 * Main Application
 */
object Server extends App {
  val system = ActorSystem("CounterRing")
  val first = system.actorOf(Props[CounterNode], name = "counter1")
  val second = system.actorOf(Props[CounterNode], name = "counter2")
  val third = system.actorOf(Props[CounterNode], name = "counter3")
  println(first.path)
  println(second.path)
  println(third.path)
  first ! Init(List(second, third, first))
  first ! TOKEN
  println("Server ready")
}
