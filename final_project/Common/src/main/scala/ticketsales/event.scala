package ticketsales

import java.util.Date
import java.text.SimpleDateFormat

class Event(n: String, d: Date, v: String) extends Serializable {
  
  val name: String = n
  val date: Date = d
  val venue: String = v

  override def toString = name + " | " + venue + " | " + Event.dateFormat.format(date)
  
  override def equals(other: Any) = other match {
    case that: Event => (this.name equals that.name) && (this.date equals that.date) &&
                        (this.venue equals that.venue)
    case _ => false
  }
                        
  override def hashCode = {
    var result = 17
    result = 41*result + name.hashCode
    result = 41*result + date.hashCode
    41*result + venue.hashCode
  }
}

object Event {

  val dateFormat = new SimpleDateFormat("yyyy-MM-dd")
  val eventFormatStr = "(.+) \\| (.+) \\| (.+)".r

  def parse(s: String): Event = {
    val eventFormatStr(name, venue, dateStr) = s
    new Event(name, dateFormat.parse(dateStr), venue)
  }  
}

