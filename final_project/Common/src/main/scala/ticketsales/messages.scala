package ticketsales

case object EVENTS

/**
 * Initialization message is passed around the ring on startup to inform
 * each node of its next-neighbors.
 */
case class EventList(events: List[Event])

case class Buy(event: Event) {
  val orderNum = Buy.nextNumber
}

object Buy {

  var globalOrderNum = 0

  def nextNumber(): Int = {
    globalOrderNum += 1
    globalOrderNum
  }    

}

case class Confirmation(event: Event, orderNum: Int)
case class Reject(event: Event, orderNum: Int)
case class Pending(event: Event, orderNum: Int)
case class Dist(event: Event, localInv: Array[Int], chunkSize: Int)
