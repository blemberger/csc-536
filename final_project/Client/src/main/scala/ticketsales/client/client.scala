package ticketsales 

import akka.actor.{Actor, ActorRef, ActorSystem, Props, ActorSelection}
import com.typesafe.config.ConfigFactory
import scala.util.Random
import scala.io.Source
import scala.collection.mutable.{Queue, Map}

object Client extends App {

  if (args.length > 0) {
    val lines = Source.fromFile(args(0)).getLines()

    val system = ActorSystem("TicketSales", ConfigFactory.load.getConfig("client"))
    val processor = system.actorOf(Props(classOf[ClientProcessor], lines), name = "client")
  }
  else 
    Console.err.println("Please supply a filename containing ticket purchase requests")

}

class ClientProcessor(orders: Iterator[String]) extends Actor { 

  // This client's kiosk actor on the Server
  var kiosk: ActorRef = null
  val kioskPath = findKiosk()
  var pendingOrders: Map[Event, Queue[Buy]] = Map.empty

  kioskPath ! EVENTS
  println("Client will use kiosk " + kioskPath.pathString) 

  def receive = {
    case EventList(events) =>  if (!events.isEmpty) {
                                 kiosk = sender
                                 purchaseEvent(events)
                               }
                               else checkShutdown()

    case Confirmation(event, orderNum) => println("Order " + orderNum + " fulfilled. " +
                                                  "1 ticket for " + event.name + " purchased.")
                                          orderComplete(event)
                                          kiosk ! EVENTS
    case Reject(event, orderNum) =>       println("Order " + orderNum + " rejected. " +
                                                  "Sorry, " + event.name + " is sold out.")
                                          orderComplete(event)
                                          kiosk ! EVENTS
    case Pending(event, orderNum) =>      println("Order " + orderNum + " is pending at kiosk " + 
                                          kiosk.path.name + ".")
                                          kiosk ! EVENTS
  }

  private def checkShutdown(): Unit = {
    println("*** " + pendingOrders.values.mkString("|"))
    if (pendingOrders.values.forall((q: Queue[Buy]) => q.isEmpty)) {
      context.stop(self)
      context.system.shutdown
    }
  }

  private def orderComplete(event: Event): Unit = {
    val pendingQ = pendingOrders(event)                              
    pendingQ.dequeue
  }

  private def findKiosk(): ActorSelection = {
    val numKiosks = ConfigFactory.load.getInt("number-kiosks")
    val kioskNumber = new Random().nextInt(numKiosks) + 1
    context.actorSelection("akka.tcp://TicketSales@127.0.0.1:2557/user/master/kiosk_" + kioskNumber)
  }

  private def purchaseEvent(events: List[Event]): Unit = {
    if (orders.hasNext) {
      val l = orders.next
        try {
          val event: Event = Event.parse(l)
          if (events.contains(event)) {
            val ordersQ = pendingOrders.getOrElseUpdate(event, new Queue())
            val b = Buy(event)
            ordersQ.enqueue(b)
            kiosk ! b
          }
          else {
            println("Sorry, " + event + " is not available.")
            kiosk ! EVENTS
          }
        } catch {
          case ex: MatchError => Console.err.println("Unable to parse as Event: " + l) 
                                 kiosk ! EVENTS
        }
    }
    else
      checkShutdown()
  }
}
