package ticketsales

import scala.collection.mutable.{Map, HashMap}
import java.util.Date
import java.text.SimpleDateFormat

abstract class Inventory {

  val DATE_FORMAT: SimpleDateFormat = new SimpleDateFormat("yyyy-MM-dd")
  protected val invMap: Map[Event, Int] = createInventory
  private val publishedMap: Map[Event, Int] = Map.empty
  
  private def isAvailable(e: Event, qty: Int): Boolean = invMap(e) >= qty

  final def availableEvents(): Iterable[Event] = invMap.keys
  
  final def remove(e: Event, qty: Int): Boolean = if (isAvailable(e, qty)) {
                                            invMap(e) = invMap(e) - qty
                                            true
                                          }
                                          else false

  def removeEvent(e: Event): Unit = invMap.remove(e)

  final def quantity(e: Event): Int = invMap.getOrElse(e, 0)

  protected def createInventory(): Map[Event, Int]

  def add(e: Event, qty: Int): Unit = if (invMap.contains(e)) 
                                        invMap(e) = invMap(e) + qty
                                      else invMap += e -> qty

  def publishInventory(e: Event): Unit = publishedMap += e -> invMap(e)

  def getPublishedInventory(e: Event): Int = publishedMap(e)
}

class KioskInventory extends Inventory {

  protected def createInventory(): Map[Event, Int] = new HashMap[Event, Int]

  def purchaseTicket(e: Event): Boolean = remove(e, 1)

  def borrow(e: Event): Unit = invMap(e) = invMap(e) - 1

  def unBorrow(e: Event): Unit = invMap(e) = invMap(e) + 1

}

class MasterInventory extends Inventory {

  protected def createInventory(): Map[Event, Int] = {
    val h = new HashMap[Event, Int]
//    h += new Event("Four Divas", DATE_FORMAT.parse("2014-08-01"), "Metro Center Amphitheater") -> 100 
//    h += new Event("Three Amigos", DATE_FORMAT.parse("2014-10-11"), "C.Y. Stephens Auditorium") -> 100
    h += new Event("Direct Current", DATE_FORMAT.parse("2014-09-23"), "Bollywood Bowl") -> 100
//    h += new Event("Monty Python Live at the Hollywood Bowl", DATE_FORMAT.parse("1980-09-26"), 
//                    "Hollywood Bowl") -> 100
  }
}
