package ticketsales

import akka.actor.ActorRef

class Order(c: ActorRef, e: Event, n: Int) {

  val orderNum = n
  val client = c
  val event = e

  override def equals(other: Any) = other match {
    case that: Order => (this.client equals that.client) && (this.event equals that.event) &&
                        (this.orderNum equals that.orderNum)
    case _ => false
  }
                        
  override def hashCode = {
    var result = 17
    result = 41*result + client.hashCode
    result = 41*result + event.hashCode
    41*result + orderNum
  }
  
  override def toString = orderNum + " | " + event + " | " + client.path
}
