package ticketsales

import scala.collection.mutable.{Map, HashMap}
import scala.util.Random
import scala.math
import akka.actor.{Actor, ActorRef, ActorSystem, Props}
import com.typesafe.config.ConfigFactory
import java.util.Date
import java.text.SimpleDateFormat

/**
 *  Master actor for the ticket sales application. Responsibilities are:
 *    1) Retrieve tickets for events from the underlying database and submit
 *       them to kiosks. 
 *    2) Watch for failures of kiosks and restart them when failures are detected.
 */ 
class TicketMaster extends Actor {

  // Number of kiosks in the application
  val numKiosks = ConfigFactory.load.getInt("number-kiosks")

  // Maximum size of a chunk of tickets that an individual kiosk can take
  val maxChunkSize = ConfigFactory.load.getInt("maximum-chunk-size")

  private val inventory: Inventory = new MasterInventory

  private var kioskSendee = self
  private val kiosks = for (kn <- numKiosks to 1 by -1) yield {
                  kioskSendee = context.actorOf(Props(classOf[TicketKiosk], kn, kioskSendee),
                                                 name = "kiosk_" + kn)
                  kioskSendee
               }

  kiosks.foreach((a: ActorRef) => println("Created kiosk: " + a.path))

  private val sendee = kiosks.last

//  val testEvnt = inventory.availableEvents.head
  inventory.availableEvents.foreach((e: Event) => 
      distribute(e, new Array[Int](numKiosks + 1)))
  
  private def distribute(event: Event, invStore: Array[Int]): Unit = {
    inventory.add(event, invStore(0))
    invStore(0) = 0
    val newTickets = math.min(numKiosks * maxChunkSize, inventory.quantity(event))
    if (inventory.remove(event, newTickets))
      invStore(0) = newTickets
      sendee ! Dist(event, invStore, maxChunkSize)
  }

  def receive = {
    case Dist(event, invStore, _) => println(self.path.name + ": " + "Dist from: " + sender.path.name +
                                             " for " + event.name + " and now have " +
                                             inventory.quantity(event) + " tickets " +
                                             "[" + invStore.mkString(", ") + "]")
                                     Thread.sleep(Server.calculateDelay(5000, 1000))
                                     distribute(event, invStore)
  }
}

/**
 * Main Application
 */
object Server extends App {
  val system = ActorSystem("TicketSales", ConfigFactory.load.getConfig("server"))
    val master = system.actorOf(Props[TicketMaster], name = "master")
    println(master.path)
    println("Server ready")

    /**
     * Calculate a random Int between minDelay and maxDelay for
     * the purpose of delaying the sending of a message.
     */
    def calculateDelay(maxDelay: Int, minDelay: Int): Int = {
      new Random().nextInt(maxDelay-minDelay) + minDelay 
    }
}

