package ticketsales

import akka.actor.{Actor, ActorRef, ActorSystem, Props}
import scala.collection.mutable.{Queue, Map}

/**
 *  Kiosk actor for the ticket sales application. Responsibilities are:
 *    1) Take and fulfill requests for tickets for events from clients.
 *    2) Share tickets with other kiosks using a ring-based message passing topology.
 */ 
class TicketKiosk(kioskNum: Int, nextActor: ActorRef) extends Actor {

  private val inventory: KioskInventory = new KioskInventory

  private val queuedOrdersByEvent: Map[Event, Queue[Order]] = Map.empty[Event, Queue[Order]]

  def receive = {
    case Dist(event, localInv, chunkSize) => reconcile(event, localInv)
                                             replenish(event, localInv, chunkSize)
                                             val modInv = shareInventory(event, localInv)
                                             //val modInv = localInv
                                             //checkRemoveEvent(event, localInv)
                                             updateInventoryMessage(event, modInv)
                                             println(self.path.name + ": " + "Dist from: " + sender.path.name +
                                                     " for " + event.name + " and now have " +
                                                     inventory.quantity(event) + " tickets " +
                                                     "[" + modInv.mkString(", ") + "]")
                                             Thread.sleep(Server.calculateDelay(5000, 1000))
                                             nextActor ! Dist(event, modInv, chunkSize)
    case b @ Buy(event) => attemptPurchase(new Order(sender, event, b.orderNum))
    case EVENTS => sender ! EventList(inventory.availableEvents.toList)
  }

  private def attemptPurchase(order: Order): Unit = {
    if (inventory.purchaseTicket(order.event)) {
      order.client ! Confirmation(order.event, order.orderNum)
    }
    else {
      inventory.borrow(order.event)
      val queuedOrders = queuedOrdersByEvent.getOrElseUpdate(order.event, new Queue())
      queuedOrders.enqueue(order)
      order.client ! Pending(order.event, order.orderNum)
    }
  }
  
  private def updateInventoryMessage(e: Event, inv: Array[Int]) = {
    inv(kioskNum) = inventory.quantity(e)
    inventory.publishInventory(e)
  }

  /* Meant to be called when a kiosk recieves the Dist message which can potentially change
   * this kiosk's inventory for an event.
   *
   * Precondition: Dist message supplies an event, an array of other kiosks and master inventory, and chunksize
   * Postconditions: 1) If local inventory for the event was zero and the Dist message had inventory, then
   *                    local inventory will be > 0
   *                 2) If local inventory for the event was zero and the Dist message had no inventory, then
   *                    local inventory will remain zero
   *                 3) If local inventory for the event was > 0, then local inventory will not be changed
   *                 4) If local inventory for the event was < 0, and the Dist message had inventory, then
   *                    all pending sales are finalized until either local inventory is zero, in which case then
   *                    1) or 2) will happen, or until the Dist message is out of inventory, in which case 5) will
   *                    happen.
   */
  private def replenish(event: Event, otherInv: Array[Int], chunkSize: Int) = {

    // Clear up any pending orders if any tickets are available
    while (inventory.quantity(event) < 0 && otherInv(0) > 0) {
      confirmQueuedPurchase(queuedOrdersByEvent(event))
      otherInv(0) -= 1
    }

    var takeSize = math.min(otherInv(0), chunkSize) // Amount still available from the Dist message

    // If any tickets are still left to take and we are out, add them to our local inventory
    if (takeSize > 0 && inventory.quantity(event) == 0)  {
      println(self.path.name + ": About to take " + takeSize + " from " + 
               otherInv(0))
      inventory.add(event, takeSize)
      otherInv(0) -= takeSize
    }
  }

  /* Confirm sales of inventory supplied by other kiosks, and bring this kiosks local inventory into
   * correspondence with any inventory supplied by another kiosk
   * kiosk can fulfill any requests, so reject any clients waiting on this event
   *
   * Postcondition: If there are pending orders for an event, and the Dist message had no inventory, then
   *                all pending sales are finalized until local inventory is zero, then the event is declared
   *                sold-out.
   */
  private def reconcile(event: Event, otherInv: Array[Int]) = {
    val queuedOrders = queuedOrdersByEvent.getOrElse(event, new Queue())
    if (!queuedOrders.isEmpty) {
      var othersFulfilled = otherInv(kioskNum) - inventory.getPublishedInventory(event)
      println("** Found " + queuedOrders.size + " queuedOrders during reconcile:")
      println("** otherInv("+ kioskNum + "): " + otherInv(kioskNum))
      println("** inventory.publishedinventory: " + inventory.getPublishedInventory(event))
      while(othersFulfilled > 0) {
        confirmQueuedPurchase(queuedOrders)
        othersFulfilled -= 1
      }
    }
  }
  
  private def shareInventory(event: Event, otherInv: Array[Int]): Array[Int] = {
    if (otherInv(0) > 0) // only share inventory if the master is completely out of tickets
      otherInv
    val modifiedInv: Array[Int] = for (qty: Int <- otherInv) yield {
      val myInv = inventory.quantity(event)
      if (qty < 0 && myInv > 0) {
        if (myInv + qty >= 0) { // this kiosk has enough to completely make up another's deficit
          inventory.add(event, myInv + qty) // reduce this kiosk by the deficit amount
          0 // the other kiosk is now back to 0
        }
        else { // this kiosk only has some of the other's deficit
          inventory.add(event, 0) // bring this kiosk's inventory down to 0
          qty + myInv // reduce other kiosk by this kiosk's entire inventory 
        }
      }
      else qty
    }
    modifiedInv
  }

  /* Reject any queued orders remaining
   *
   * Precondition: queuedOrders is not empty and no inventory for the event remains in the system
   * CALL FROM checkRemoveEvent()
   */
  private def rejectUnfulfilled(queuedOrders: Queue[Order]) = {
    while (!queuedOrders.isEmpty) {
      val queuedOrder = queuedOrders.dequeue
      queuedOrder.client ! Reject(queuedOrder.event, queuedOrder.orderNum)
      inventory.unBorrow(queuedOrder.event)
    }
  }

  private def confirmQueuedPurchase(queuedOrders: Queue[Order]) = {
    val queuedOrder = queuedOrders.dequeue
    queuedOrder.client ! Confirmation(queuedOrder.event, queuedOrder.orderNum)
    inventory.unBorrow(queuedOrder.event)
  }
}
