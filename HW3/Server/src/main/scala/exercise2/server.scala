package exercise2

import akka.actor.{ActorSystem, ActorRef, Props}
import com.typesafe.config.ConfigFactory

object ProperNameCountServer extends App {

  val system = ActorSystem("ProperNameCountRemote",
                           ConfigFactory.load.getConfig("server"))

}
