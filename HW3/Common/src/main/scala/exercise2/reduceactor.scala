package exercise2

import scala.collection.mutable.HashMap

import akka.actor.{Actor, ActorRef}
import com.typesafe.config.ConfigFactory

class ReduceActor extends Actor {
  var remainingMappers = ConfigFactory.load.getInt("number-mappers")
  var reduceMap = HashMap[String, List[String]]()

  def receive = {
    case ProperName(word, title) =>
      val existingTitles = reduceMap.getOrElseUpdate(word, Nil)
      // Have to put a new list back, because Lists are immutable
      reduceMap(word) = title :: existingTitles 
    case EOL => 
      remainingMappers -= 1
      if (remainingMappers == 0) {
        var msg: String = "\n" + "*" * 10
        msg += " " + self.path.toStringWithoutAddress + " :\n"
        reduceMap.foreach((entry) => msg += entry._1 + " : " + entry._2.mkString(", ")
                           + "\n")
        print(msg)
        context.actorSelection("../..") ! Done
        context stop self
      }
  }
}
