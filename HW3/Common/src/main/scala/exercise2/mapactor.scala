package exercise2

import akka.actor.{Actor, ActorRef, ActorSelection}
import akka.routing.Broadcast
import scala.io.Source

class MapActor(reduceActorsRouter: ActorRef) extends Actor {

  println("Mapper " + self.path.name + " coming alive!")

  def receive = {
    case (title: String, url: String) =>
      process(title, downloadText(url))
    case EOL => 
      reduceActorsRouter ! Broadcast(EOL)
  }

  def process(title: String, text: String) = {
    val myRx = "[A-Z][a-z]+".r // Match any capitalized word
    val nameSet = myRx.findAllIn(text).toSet // produce Set (no repetitions)
    for (name <- nameSet) {
      reduceActorsRouter ! ProperName(name, title) 
    }
  }

  def downloadText(url: String): String = {
    return Source.fromURL(url).mkString
  }

}
