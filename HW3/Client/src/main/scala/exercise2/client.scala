package exercise2

import akka.actor.{ActorSystem, Props}
import com.typesafe.config.ConfigFactory

object ProperNameCountClient extends App {

  val system = ActorSystem("ProperNameCountRemote",
                           ConfigFactory.load.getConfig("remotelookup"))
  val master = system.actorOf(Props[MasterActor], name = "master")

  val URLPREFIX = "http://reed.cs.depaul.edu/lperkovic/csc536/homeworks/gutenberg/"

  val texts = Array(("Bleak House", "pg1023.txt"),
                    ("Great Expectations", "pg1400.txt"),
                    ("A Christmas Carol", "pg19337.txt"),
                    ("The Cricket on the Hearth", "pg20795.txt"),
                    ("The Pickwick Papers", "pg580.txt"),
                    ("A Child's History of England", "pg699.txt"),
                    ("The Old Curiosity Shop", "pg700.txt"),
                    ("Oliver Twist", "pg730.txt"),
                    ("David Copperfield", "pg766.txt"),
                    ("Hunted Down", "pg807.txt"),
                    ("Dombey and Son", "pg821.txt"),
                    ("Our Mutual Friend", "pg883.txt"),
                    ("Little Dorrit", "pg963.txt"),
                    ("The Life And Adventures Of Nicholas Nickleby", "pg967.txt"),
                    ("Life And Adventures Of Martin Chuzzlewit", "pg968.txt"),
                    ("A Tale of Two Cities", "pg98.txt"))

  // send (title, URL) messages to the master actor
  for (book <- texts)
    master ! (book._1, URLPREFIX + book._2)
  master ! EOL 
}
