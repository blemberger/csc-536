package exercise2

import akka.actor.{Actor, ActorRef, Props, ActorSelection, Address}
import akka.routing.{Broadcast, RoundRobinPool, ConsistentHashingPool}
import akka.remote.routing.RemoteRouterConfig

import com.typesafe.config.ConfigFactory

class MasterActor extends Actor {

  val numberMappers  = ConfigFactory.load.getInt("number-mappers")
  val numberReducers  = ConfigFactory.load.getInt("number-reducers")
  var pending = numberReducers

  val reducerHashing: PartialFunction[Any, String] = {
    case ProperName(word, title) => word
  }

  val addresses = Seq(Address("akka.tcp", "ProperNameCountRemote", "127.0.0.1", 2552),
                      self.path.address) // Add the local client as well

  val reduceActors = context.actorOf(RemoteRouterConfig(
                                      ConsistentHashingPool(numberReducers,
                                        hashMapping = reducerHashing ), addresses)
                                      .props(Props(classOf[ReduceActor])))

  val mapActors = context.actorOf(
                    RemoteRouterConfig(RoundRobinPool(numberMappers), addresses).props(
                                         Props(classOf[MapActor], reduceActors)))
  def receive = {
    case msg: (_, _) =>
      mapActors ! msg
    case EOL =>
      mapActors ! Broadcast(EOL)
    case Done => pending -= 1
                 if (pending == 0) 
                   context.system.shutdown
    }
  
}
