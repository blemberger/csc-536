package exercise1

import akka.actor.{Actor, ActorRef}
import scala.io.Source

class MapActor(reduceActors: List[ActorRef]) extends Actor {

  val numReducers = reduceActors.size

  def receive = {
    case (title: String, url: String) =>
      process(title, downloadText(url))
    case EOL => 
      for (i <- 0 until numReducers) {
        reduceActors(i) ! EOL 
      }
  }

  def process(title: String, text: String) = {
    val myRx = "[A-Z][a-z]+".r // Match any capitalized word
    val nameSet = myRx.findAllIn(text).toSet // produce Set (no repetitions)
    for (name <- nameSet) {
      var index = Math.abs((name.hashCode()) % numReducers)
      reduceActors(index) ! ProperName(name, title)
    }
  }

  def downloadText(url: String): String = {
    return Source.fromURL(url).mkString
  }
}
