package watcher

import akka.actor.{Actor, ActorSystem, Props, Terminated}
import com.typesafe.config.ConfigFactory

object DeathWatchClient extends App {

  val system = ActorSystem("DeathWatch",
                           ConfigFactory.load.getConfig("remotelookup"))

  val watcher = system.actorOf(Props[WatchActor], name = "watcher")
}

class WatchActor extends Actor {

  val subject = context.actorSelection("akka.tcp://DeathWatch@127.0.0.1:2552/user/subject")
  subject ! Message("Hello")

  def receive = {
    case msg @ Message(_) => subject ! msg

    case MessageAck(msg) => println("Got a valid acknowledgement: %s".format(msg))
                            context.watch(sender)

    case Terminated(_) =>
        println("The subject actor appears to have shut down. Terminating ActorSystem.")
        DeathWatchClient.system.shutdown()
  }
}
