package watcher

case class Message(msg: String)
case class MessageAck(msg: String)
