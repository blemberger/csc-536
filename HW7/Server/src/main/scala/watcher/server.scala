package watcher

import akka.actor.{Actor, ActorSystem, Props}
import com.typesafe.config.ConfigFactory

object DeathWatchServer extends App {

  val system = ActorSystem("DeathWatch",
                           ConfigFactory.load.getConfig("server"))

  system.actorOf(Props[SubjectActor], name = "subject")
}

class SubjectActor extends Actor {

  println("Subject Actor starting up.")

  def receive = {
    case Message(msg) => println("Got a valid message: %s".format(msg))
                         sender ! MessageAck(msg)
                         println("Type Ctrl-C to shut down.")
    case default => println("Got a message I don't understand.")
  }
  
}
